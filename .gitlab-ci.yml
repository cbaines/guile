variables:
  GIT_DEPTH: 100
  GIT_SUBMODULE_STRATEGY: normal

default:
  interruptible: true
  artifacts:
    expire_in: 2 weeks
    when: always
    paths:
    - "*.tar.gz"
    - ./*.log
    - ./config.h
    - ./**/*.log

Trisquel10-git:
  image: kpengboy/trisquel:10.0
  stage: build
  before_script:
  - apt-get update -q
  - apt-get install -qqy git make gcc guile-3.0-dev libgnutls28-dev texinfo
  script:
  - ./bootstrap
  - ./configure
  - make syntax-check
  - make -j$(nproc) check V=1 VERBOSE=t
  - make dist
  - git diff --exit-code # nothing should change version controlled files

PureOS10-git:
  image: pureos/byzantium:latest
  stage: build
  before_script:
  - apt-get update -q
  - apt-get install -qqy git make gcc guile-3.0-dev libgnutls28-dev texinfo
  script:
  - ./bootstrap
  - ./configure
  - make syntax-check
  - make -j$(nproc) check V=1 VERBOSE=t
  - make dist
  - git diff --exit-code # nothing should change version controlled files

Debian11-git:
  image: debian:11
  stage: build
  before_script:
  - apt-get update -q
  - apt-get install -qqy git make gcc guile-3.0-dev libgnutls28-dev texinfo texlive
  script:
  - git submodule deinit --all # make sure we can bootstrap without gnulib submodule
  - ./bootstrap
  - ./configure
  - make syntax-check
  - make -j$(nproc) distcheck V=1 VERBOSE=t
  - git diff --exit-code # nothing should change version controlled files

AlmaLinux8-git:
  image: almalinux:8
  stage: build
  before_script:
  - dnf -y --enablerepo=powertools install git autoconf automake libtool make guile-devel gnutls-devel texinfo patch diffutils
  script:
  - ./bootstrap
  - ./configure
# FIXME XXX  - make syntax-check
  - make check V=1 VERBOSE=t
  - git diff --exit-code # nothing should change version controlled files

Fedora36-git:
  image: fedora:36
  stage: build
  before_script:
  - yum -y update
  - yum -y install git autoconf automake libtool make guile-devel gnutls-devel texinfo indent patch diffutils
  script:
  - ./bootstrap
  - ./configure
  - make syntax-check
  - make check V=1 VERBOSE=t
  - make dist
  - git diff --exit-code # nothing should change version controlled files

Ubuntu22.04-release:
  image: ubuntu:22.04
  stage: build
  except:
    - tags # do-release-commit-and-tag: not on branch
  before_script:
  - apt-get update -q | tail
  - apt-get install -qqy git make gcc guile-3.0-dev libgnutls28-dev texinfo indent | tail
  script:
  - git submodule deinit --all # make sure we can bootstrap without gnulib submodule
  - ./bootstrap
  - ./configure
  - make syntax-check
  - make
  - make dist
  - git diff --exit-code # nothing should change version controlled files
  - apt-get install -qqy texlive gpg gpgv2 | tail
  - gpg --batch --passphrase '' --quick-gen-key gnutls-help@lists.gnutls.org
  - git checkout -B my-release-branch $CI_COMMIT_SHA
  - git config user.email "gnutls-help@lists.gnutls.org"
  - git config user.name "Guile-GnuTLS CICD Builder"
  - git config user.signingkey gnutls-help@lists.gnutls.org
  - sed -i '5i ** WARNING This release was prepared automatically with no testing.\n' NEWS
  - git commit -m "Warn about automatic release." NEWS
  - make release-commit RELEASE='17.42.23 stable'
  - make
  - make release RELEASE='17.42.23 stable' V=1 VERBOSE=t
  - cat -n ~/announce-guile-gnutls-17.42.23
  - git diff --exit-code # nothing should change version controlled files

Trisquel10-tarball:
  image: kpengboy/trisquel:10.0
  stage: test
  needs: [Trisquel10-git]
  before_script:
  - apt-get update -q
  - apt-get install -qqy make gcc guile-3.0-dev libgnutls28-dev
  script:
  - tar xfa guile-gnutls-*.tar.gz
  - cd `ls -d guile-gnutls-* | grep -v tar.gz`
  - ./configure
  - make check V=1 VERBOSE=t

Debian10-tarball-autoreconf:
  image: debian:10
  stage: test
  needs: [Trisquel10-git]
  before_script:
  - apt-get update -q
  - apt-get install -qqy make gcc guile-2.2-dev libgnutls28-dev
  - apt-get install -qqy autoconf
  script:
  - tar xfa guile-gnutls-*.tar.gz
  - cd `ls -d guile-gnutls-* | grep -v tar.gz`
  - autoreconf -fvi
  - ./configure
  - grep "^VERSION='UNKNOWN'" config.log && exit 1
  - make check V=1 VERBOSE=t

Alpine-tarball:
  image: alpine:latest
  stage: test
  needs: [Trisquel10-git]
  before_script:
  - apk update
  - apk add build-base guile-dev gnutls-dev
  script:
  - tar xfa guile-gnutls-*.tar.gz
  - cd `ls -d guile-gnutls-* | grep -v tar.gz`
  - ./configure
  - make check V=1 VERBOSE=t

ArchLinux-tarball:
  image: archlinux:latest
  stage: test
  needs: [Trisquel10-git]
  before_script:
  - pacman -Syu --noconfirm make gcc pkgconf guile gnutls
  script:
  - tar xfa guile-gnutls-*.tar.gz
  - cd `ls -d guile-gnutls-* | grep -v tar.gz`
  - ./configure
  - make check V=1 VERBOSE=t

Fedora36-tarball:
  image: fedora:36
  stage: test
  needs: [Trisquel10-git]
  before_script:
  - yum -y update
  - yum -y install make clang guile-devel gnutls-devel
  script:
  - tar xfa guile-gnutls-*.tar.gz
  - cd `ls -d guile-gnutls-* | grep -v tar.gz`
  - ./configure
  - make check V=1 VERBOSE=t

Ubuntu22.04-tarball:
  image: ubuntu:22.04
  stage: test
  needs: [Trisquel10-git]
  before_script:
  - apt-get update -q
  - apt-get install -qqy make gcc guile-3.0-dev libgnutls28-dev
  script:
  - tar xfa guile-gnutls-*.tar.gz
  - cd `ls -d guile-gnutls-* | grep -v tar.gz`
  - ./configure
  - make check V=1 VERBOSE=t

web-manual:
  image: fedora:latest
  stage: build
  before_script:
  - yum -y update
  - yum -y install git autoconf automake libtool make guile-devel gnutls-devel texinfo patch
  - yum -y install texlive texinfo-tex
  script:
  - ./bootstrap
  - ./configure
  - make
  - make web-manual
  - git diff --exit-code # nothing should change version controlled files
  artifacts:
    when: on_success
    paths:
      - doc/manual

.pages:
  stage: deploy
  needs: ["web-manual"]
  script:
    - mkdir public
    - mv doc/manual public/manual
  artifacts:
    paths:
    - public
    expire_in: 30 days

pages:
  extends: .pages
  only:
    - master

pages-test:
  extends: .pages
  except:
    - master
